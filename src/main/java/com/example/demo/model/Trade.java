package com.example.demo.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;

@Component
public class Trade {
	@Id
	public ObjectId _id;
	private Date date;
	private String stockSymbol;
	private double currentValue;
	private int quantity;
	private double requestedPrice;
	public static enum status {
		CREATED, PENDING, CANCELLED, REJECTED, FILLED, PARTIALLY_FILLED, ERROR
	};
	private status tradeStatus;
	
	
	public Trade() {}
	
	public Trade(ObjectId _id, Date date, String stockSymbol, double currentValue, int quantity, double requestedPrice,
			status tradeStatus) {
		this._id = _id;
		this.date = date;
		this.stockSymbol = stockSymbol;
		this.currentValue = currentValue;
		this.quantity = quantity;
		this.requestedPrice = requestedPrice;
		this.tradeStatus =  tradeStatus;
	}

	public String get_id() { return _id.toHexString(); }
	
	public void set_id(ObjectId _id) { this._id = _id; }
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getRequestedPrice() {
		return requestedPrice;
	}
	public void setRequestedPrice(double requestedPrice) {
		this.requestedPrice = requestedPrice;
	}

	public String getStockSymbol() {
		return stockSymbol;
	}

	public void setStockSymbol(String stockSymbol) {
		this.stockSymbol = stockSymbol;
	}

	public double getCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(double currentValue) {
		this.currentValue = currentValue;
	}

	public status getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(status tradeStatus) {
		this.tradeStatus = tradeStatus;
	}
	
	
	
}
