package com.example.demo.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.model.Trade;

public interface TradeRepository extends MongoRepository<Trade, String>{
	Trade findBy_id(ObjectId _id);
}
